﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour
{
	public Transform mainCamera;
	public Transform endPos;
	Vector3 idlePos;
	Quaternion idleRot;
	public bool moveToHouse;
	public float pos = 0.2f;
	public float degree = 0.2f;
	
	void Start () 
	{
		idlePos = mainCamera.position;
		idleRot = mainCamera.rotation;
	}

	void Update () 
	{
		Vector3 targetPos;
		Quaternion targetRot;

		if (moveToHouse) 
		{
			targetPos = endPos.position;
			targetRot = endPos.rotation;
		}
		else 
		{
			targetPos = idlePos;
			targetRot = idleRot;
		}

		mainCamera.position = Vector3.MoveTowards(mainCamera.position, targetPos, pos);
		mainCamera.rotation = Quaternion.RotateTowards (mainCamera.rotation, targetRot, degree);	
	}
}
